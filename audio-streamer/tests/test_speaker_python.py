import argparse

import alsaaudio
import numpy as np

# ==================================================================================================


volume = 0.3  # range [0.0, 1.0]
duration = 10.0  # in seconds, may be float
frequency = 440.0  # sine frequency, Hz, may be float
sample_rate = 48000
write_buffer_size = 160


# ==================================================================================================


def main():
    parser = argparse.ArgumentParser(description="Test speakers")
    parser.add_argument(
        "--device_name",
        default="default",
        type=str,
        help="ALSA device name of the speakers",
    )
    args = parser.parse_args()

    # Generate samples
    samples = 2 * np.pi * np.arange(sample_rate * duration) * frequency
    samples = np.sin(samples / sample_rate)
    samples = (samples * volume).astype(np.float32)

    # Convert to int16 format
    samples = samples * np.iinfo(np.int16).max
    samples = samples.astype(np.int16)

    # Create interface to audio device
    streamer = alsaaudio.PCM(
        alsaaudio.PCM_PLAYBACK,
        channels=1,
        rate=sample_rate,
        format=alsaaudio.PCM_FORMAT_S16_LE,
        periodsize=write_buffer_size,
        device=args.device_name,
    )

    # Play the sample array in chunks
    idx = 0
    while idx < len(samples):
        old_idx = idx
        idx = min(idx + write_buffer_size, len(samples))
        buff = samples[old_idx:idx]

        streamer.write(buff.tobytes())


# ==================================================================================================

if __name__ == "__main__":
    main()
